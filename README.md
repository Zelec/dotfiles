# Isaac's .dotfiles

Clone this repo via SSH,

```bash
git clone --bare git@gitlab.com:Zelec/dotfiles.git $HOME/.dotfiles
```

or HTTPS,
```bash
git clone --bare https://gitlab.com/Zelec/dotfiles.git $HOME/.dotfiles
```

Then to setup:
```bash
mkdir -p .dotfiles-backup
git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} mv {} .dotfiles-backup/{}
git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME checkout
git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME config --local status.showUntrackedFiles no
git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME config --local pull.ff only
reset
```

use the git wrapper command `config` to manage changes afterwards

Links:  
- https://www.atlassian.com/git/tutorials/dotfiles  
- https://wiki.archlinux.org/index.php/Dotfiles  
