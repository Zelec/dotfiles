#
# shellcheck disable=SC2148
# shellcheck disable=SC2145
# shellcheck disable=SC1090
# shellcheck disable=SC1091
#

[[ $- != *i* ]] && return

[ -r /usr/share/bash-completion/bash_completion ] && . /usr/share/bash-completion/bash_completion

# Change the window title of X terminals
case ${TERM} in
	xterm*|rxvt*|Eterm*|aterm|kterm|gnome*|interix|konsole*)
		PROMPT_COMMAND='echo -ne "\033]0;${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\007"'
		;;
	screen*)
		PROMPT_COMMAND='echo -ne "\033_${USER}@${HOSTNAME%%.*}:${PWD/#$HOME/\~}\033\\"'
		;;
esac

use_color=true

# Set colorful PS1 only on colorful terminals.
# dircolors --print-database uses its own built-in database
# instead of using /etc/DIR_COLORS.  Try to use the external file
# first to take advantage of user additions.  Use internal bash
# globbing instead of external grep binary.
safe_term=${TERM//[^[:alnum:]]/?}   # sanitize TERM
match_lhs=""
[[ -f ~/.dir_colors   ]] && match_lhs="${match_lhs}$(<~/.dir_colors)"
[[ -f /etc/DIR_COLORS ]] && match_lhs="${match_lhs}$(</etc/DIR_COLORS)"
[[ -z ${match_lhs}    ]] \
	&& type -P dircolors >/dev/null \
	&& match_lhs=$(dircolors --print-database)
[[ $'\n'${match_lhs} == *$'\n'"TERM "${safe_term}* ]] && use_color=true

if ${use_color} ; then
	# Enable colors for ls, etc.  Prefer ~/.dir_colors #64489
	if type -P dircolors >/dev/null ; then
		if [[ -f ~/.dir_colors ]] ; then
			eval "$(dircolors -b ~/.dir_colors)"
		elif [[ -f /etc/DIR_COLORS ]] ; then
			eval "$(dircolors -b /etc/DIR_COLORS)"
		fi
	fi

	if [[ ${EUID} == 0 ]] ; then
		PS1='\[\033[01;31m\][\h\[\033[01;36m\] \W\[\033[01;31m\]]\$\[\033[00m\] '
	else
		PS1='\[\033[01;36m\][\u@\h\[\033[01;37m\] \W\[\033[01;36m\]]\$\[\033[00m\] '
	fi

	alias ls='ls --color=auto'
	alias grep='grep --colour=auto'
	alias egrep='egrep --colour=auto'
	alias fgrep='fgrep --colour=auto'
else
	if [[ ${EUID} == 0 ]] ; then
		# show root@ when we don't have colors
		PS1='\u@\h \W \$ '
	else
		PS1='\u@\h \w \$ '
	fi
fi

# Environment exports
export EDITOR=nano
export DOCKER_BUILDKIT=1
export COMPOSE_DOCKER_CLI_BUILD=1

unset use_color safe_term match_lhs sh

xhost +local:root > /dev/null 2>&1

# Bash won't get SIGWINCH if another process is in the foreground.
# Enable checkwinsize so that bash will check the terminal size when
# it regains control.  #65623
# http://cnswww.cns.cwru.edu/~chet/bash/FAQ (E11)
shopt -s checkwinsize

shopt -s expand_aliases

# export QT_SELECT=4

# Enable history appending instead of overwriting.  #139609
shopt -s histappend

if ! pgrep -u "$USER" ssh-agent > /dev/null; then
  ssh-agent -t 1h > "$XDG_RUNTIME_DIR/ssh-agent.env"
fi
if [[ ! "$SSH_AUTH_SOCK" ]]; then
  [ -r "$XDG_RUNTIME_DIR/ssh-agent.env" ] && source "$XDG_RUNTIME_DIR/ssh-agent.env" >/dev/null
fi

# Gitignore creator
gi() { 
	curl -sL "https://www.gitignore.io/api/$@"
}

btrfs-scrub-watch() {
  sudo FOLDER="${@%/}" watch 'for dir in "$FOLDER"/*/; do echo "$dir" && btrfs scrub status "$dir" && echo ""; done'
}

btrfs-scrub-start() {
  sudo FOLDER="${@%/}" bash -c 'for dir in "$FOLDER"/*/; do btrfs scrub start "$dir"; done'
}

# dotfiles config
alias config='/usr/bin/git --git-dir=$HOME/.dotfiles/ --work-tree=$HOME'

# Container top
ctop() {
	docker pull quay.io/vektorlab/ctop:latest
	docker run --name ctop -it --rm -v /var/run/docker.sock:/var/run/docker.sock quay.io/vektorlab/ctop:latest
}

# Container upgrade via watchtower
watchtower() {
	if [ ! -f "$HOME/.docker/config.json" ]; then
		mkdir -p "$HOME/.docker"
		cat <<- EOF > "$HOME/.docker/config.json"
		{

		}
		EOF
	fi
	docker pull docker.io/containrrr/watchtower:latest
	docker run --rm -v /var/run/docker.sock:/var/run/docker.sock -v "$HOME/.docker/config.json":/config.json:ro docker.io/containrrr/watchtower:latest --run-once
}

# oxker docker tui
oxker() {
	docker pull docker.io/mrjackwills/oxker:latest
	docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock:ro docker.io/mrjackwills/oxker:latest
}

if [ -r /usr/share/bash-complete-alias/complete_alias ]; then
	. /usr/share/bash-complete-alias/complete_alias
	complete -F _complete_alias config
fi

if [ "$(find ~/.additional-sources/*.sh 2>/dev/null | wc -l)" != 0 ]; then
	for source_file in ~/.additional-sources/*.sh; do
		source "${source_file}"
	done
fi